<?php
namespace MMC\Extbasewsutility;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;


class Utility {

  protected static $imageService = NULL;
  protected static $objectManager = NULL;
  protected static $context = NULL;

  protected static function getObjectManager(){
    return self::$objectManager ?? self::$objectManager = GeneralUtility::makeInstance(ObjectManager::class);
  }

  protected static function getContext(){
    return self::$context ?? self::$context = GeneralUtility::makeInstance(Context::class);
  }

  protected static $extbaseSortByProperty;
  protected static $extbaseSortByAscending;
  protected static $findAllCache = [];
  protected static $findByAndSortCache = [];

  /**
  * isLiveWorkspace
  *
  * @return boolean true if we are in live workspace
  */
  public static function isLiveWorkspace(){
    return self::getContext()->getPropertyFromAspect('workspace', 'isLive');
  }


  /**
  * workspaceAwareExtbaseFindByAndSort
  *
  * If the current workspace is live, returns $returnIfLiveWorkspace
  * Else it will fetch all records from $repositoryClassName, filter by property/value and sort by $sortByProperty
  * workaround for improper representation of draft workspace records, e.g. inline records
  *
  * @param string $repositoryClassName
  * @param array $findBy (property name) => (value) pairs to find records by
  * @param string $sortByProperty sort result by this property (set '' / null for no sorting)
  * @param bool $sortAscending
  * @return mixed result set \TYPO3\CMS\Extbase\Persistence\ObjectStorage or $returnIfLiveWorkspace
  */
  public static function extbaseFindByAndSort( $repositoryClassName, $findBy, $sortByProperty, $sortAscending ){
    // build hash of the arguments for caching
    $args = [$repositoryClassName,  $sortByProperty, $sortAscending];
    foreach ( $findBy as $k => $v ){
      $args[] = [$k => ($v instanceof AbstractEntity) ? $v->getUid() : $v ];
    }
    $argumentsHash = md5( serialize( $args ) );
    if( isset(self::$findByAndSortCache[$argumentsHash]) ){
      return self::$findByAndSortCache[$argumentsHash];
    }
    if( !isset(self::$findAllCache[$repositoryClassName]) ){
      // just load again if this is the first call!
      self::$findAllCache[$repositoryClassName] = self::getObjectManager()->get($repositoryClassName)->findAll();
    }
    $resultFiltered = [];
    foreach(self::$findAllCache[$repositoryClassName] as $o){
      $valid = true;
      foreach($findBy as $property => $value){
        if( call_user_func([$o, 'get'.ucfirst($property)]) != $value ){
          $valid = false;
          break;
        }
      }
      if ($valid){
        $resultFiltered[] = $o;
      }
    }
    if($sortByProperty){
      self::$extbaseSortByProperty = $sortByProperty;
      self::$extbaseSortByAscending = $sortAscending;
      usort($resultFiltered, [self::class, 'usortByProperty']);
    }
    return self::$findByAndSortCache[$argumentsHash] = self::arrayToObjectStorage( $resultFiltered );
  }



  /**
  * sortObjectStorage
  *
  * Sort records by $sortByProperty
  *
  * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\DomainObject\AbstractEntity> object array to sort
  * @param string $sortByProperty sort result by this property
  * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\DomainObject\AbstractEntity> result set
  */
  public static function sortObjectStorage($objects, $sortByProperty, $sortAscending = true){
    if( self::isLiveWorkspace() ){
      return $objects;
    }
    self::$extbaseSortByProperty = $sortByProperty;
    self::$extbaseSortByAscending = $sortAscending;
    $objectsArray = $objects->getArray();
    usort($objectsArray, [self::class, 'usortByProperty']);
    return self::arrayToObjectStorage( $objectsArray );
  }


  /* ----------------- private functions -------------------- */

  private static function usortByProperty($a, $b){
    $asc = self::$extbaseSortByAscending;
    $valA = call_user_func([$asc ? $a : $b,'get'.ucfirst(self::$extbaseSortByProperty)]);
    $valB = call_user_func([$asc ? $b : $a,'get'.ucfirst(self::$extbaseSortByProperty)]);
    return $valA > $valB ? 1 : ($valA == $valB ? 0 : -1);
  }

  private static function arrayToObjectStorage($objectsArray){
    $objects = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage;
    foreach($objectsArray as $o)
      $objects->attach($o);
    $objects->_memorizeCleanState();
    return $objects;
  }

}
