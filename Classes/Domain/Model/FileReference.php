<?php
namespace MMC\Extbasewsutility\Domain\Model;

/*--------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------

Author
-------
Matthias Mächler
maechler@mm-computing.ch
http://mm-computing.ch


Date: 2020-02
----------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------*/

/**
 * @package sgpawerkdok
 */
class FileReference extends \TYPO3\CMS\Extbase\Domain\Model\FileReference {


	/**
	 * tableLocal
	 *
	 * @var string
	 */
	protected $tableLocal = 'sys_file';


	/**
	 * sortingForeign
	 *
	 * @var int
	 */
	protected $sortingForeign;


	/**
	 * Sets the uidLocal to the uid of $file
	 *
	 * @param \TYPO3\CMS\Core\Resource\FileInterface
	 */
	public function SetFile(\TYPO3\CMS\Core\Resource\FileInterface $file) {
		$this->uidLocal = (int)$file->getUid();
	}


	/**
	 * Returns the sortingForeign
	 *
	 * @return int
	 */
	public function GetSortingForeign() {
		return $this->sortingForeign;
	}


	/**
	 * Returns the tableLocal
	 *
	 * @return string
	 */
	public function GetTableLocal() {
		return $this->tableLocal;
	}


}
