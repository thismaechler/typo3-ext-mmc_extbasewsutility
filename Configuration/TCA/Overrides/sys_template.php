<?php
defined('TYPO3_MODE') || die();

(function () {

	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('mmc_extbasewsutility', 'Configuration/TypoScript', 'MMC extbase workspace utility');

})();
