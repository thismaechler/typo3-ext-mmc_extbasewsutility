<?php
declare(strict_types = 1);

return array(
  \MMC\Extbasewsutility\Domain\Model\FileReference::class => array(
    'tableName' => 'sys_file_reference'
  )
);
