<?php

$EM_CONF[$_EXTKEY] = [
  'title' => 'Extbase workspace utility',
  'description' => 'Find records in extbase workspace aware',
  'category' => 'misc',
  'author' => 'Matthias Mächler',
  'author_company' => 'mm-computing',
  'author_email' => 'maechler@mm-computing.ch',
  'state' => 'stable',
  'clearCacheOnLoad' => true,
  'version' => '1.1.3',
  'constraints' => [
    'depends' => [
      'typo3' => '9.5.0-10.4.99',
    ],
  ],
];
